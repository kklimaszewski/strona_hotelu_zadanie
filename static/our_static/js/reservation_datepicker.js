/**
 * Created by karol on 02.12.15.
 */

$(function() {
$( "#start_reservation" ).datepicker({
    minDate: 0,
    dateFormat: "yy-mm-dd",
    onClose: function(selectedDate){
        var date1 = $('#start_reservation').datepicker('getDate');
         date1.setDate(date1.getDate() + 1);
         $("#end_reservation").datepicker("option", "minDate", date1);
    }
});

$( "#end_reservation" ).datepicker({
    minDate: 1,
    dateFormat: "yy-mm-dd",
    onClose: function(selectedDate){
        $("#start_reservation").datepicker("option", "maxDate", selectedDate)
    }
});
});