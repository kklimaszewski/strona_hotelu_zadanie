from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # Examples:
    # url(r'^$', 'zadanie.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'apartment.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^reservations/edit/(?P<pk>[\d]+)/', 'reservation.views.edit', name='edit'),
    url(r'^reservations/delete/(?P<pk>[\d]+)/', 'reservation.views.delete', name='delete'),
    url(r'^reservation/room/(?P<pk>[\d]+)/', 'reservation.views.reservation', name='reservation'),
    url(r'^reservations/$', 'reservation.views.reservations', name='reservations'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
