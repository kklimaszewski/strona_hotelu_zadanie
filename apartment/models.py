from django.db import models

# Create your models here.

class Apartment(models.Model):
    room_number = models.IntegerField(unique=True)
    people = models.IntegerField()

    def __unicode__(self):
        return str(self.room_number)