from django.contrib import admin

from .models import Apartment

class ApartmentAdmin(admin.ModelAdmin):
    list_display = ["room_number", "people"]

admin.site.register(Apartment, ApartmentAdmin)
