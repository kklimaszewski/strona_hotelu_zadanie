# -*- coding: utf-8 -*-
from django.db.models import Q
from django.shortcuts import render
from apartment.models import Apartment
from reservation.forms import ReservationForm
from .models import Reservation


def reservation(request, pk):
    room_list = Apartment.objects.filter(people=pk)
    form = ReservationForm()
    context = {
            'form':form
        }

    if request.method == 'POST':
        updated_data = request.POST.copy()
        updated_data.update({'room_type':room_list[0].id})
        form = ReservationForm(data=updated_data)
        if form.is_valid():
            start_reservation = request.POST.get('start_reservation')
            end_reservation = request.POST.get('end_reservation')
            for check_room in room_list:
                find=True
                reservation_list = Reservation.objects.filter(room_type=check_room)
                for check_reservation in reservation_list:
                    reservation_check = Reservation.objects.filter((Q(start_reservation__gt=start_reservation) and Q(start_reservation__gt=end_reservation))|
                                                                   (Q(end_reservation__lt=start_reservation) and Q(end_reservation__lt=end_reservation)),
                                                                   id=check_reservation.id).exists()
                    if (reservation_check==False):
                        find=False
                        break

                if (find==True):
                    updated_data = request.POST.copy()
                    updated_data.update({'room_type':check_room.id})
                    form = ReservationForm(data=updated_data)

                    if form.is_valid():
                        title = "Dokonano rezerwacji. Dziękujemy!"
                        form.save()
                        context = {
                            'title':title,
                            'form':form
                        }
                        return render(request, 'reservation.html', context)

            title = "Termin na pokój tego typu już zarezerwowany."
            context = {
                'title':title,
                'form':form
            }
            return render(request, 'reservation.html', context)


        else:
            title = "Proszę o poprawne wypełnienie formularza."
            context = {
                'form':form,
                'title':title
            }
            return render(request, 'reservation.html', context)

    return render(request, 'reservation.html', context)



def reservations(request):
    queryset = Reservation.objects.all()
    context = {
        'queryset':queryset
    }
    return render(request, "reservations.html", context)




def edit(request, pk):
    instance = Reservation.objects.get(id=pk)
    form = ReservationForm(instance=instance)
    room_size = instance.room_type.people
    context = {
            'form':form
        }
    if request.method == 'POST':
        updated_data = request.POST.copy()
        updated_data.update({'room_type':instance.room_type.id})
        form = ReservationForm(data=updated_data)
        if form.is_valid():
            start_reservation = request.POST.get('start_reservation')
            end_reservation = request.POST.get('end_reservation')
            room_list = Apartment.objects.filter(people=room_size)
            for check_room in room_list:
                find=True
                reservation_list = Reservation.objects.filter(room_type=check_room)

                clean_reservation = [element for element in reservation_list if element.id != instance.id]

                for check_reservation in clean_reservation:



                    reservation_check = Reservation.objects.filter((Q(start_reservation__gt=start_reservation) and Q(start_reservation__gt=end_reservation))|
                                                               (Q(end_reservation__lt=start_reservation) and Q(end_reservation__lt=end_reservation)),
                                                               id=check_reservation.id).exists()
                    if (reservation_check==False):
                        find=False
                        break

                if (find==True):
                    updated_data = request.POST.copy()
                    updated_data.update({'room_type':check_room.id})
                    form = ReservationForm(data=updated_data)

                    if form.is_valid():
                        form = ReservationForm(data=updated_data, instance=instance)

                        title = "Rezerwacja edytowana. Dziękujemy!"
                        form.save()
                        context = {
                            'title':title,
                            'form':form
                        }
                        return render(request, 'reservation.html', context)

            title = "Termin na pokój tego typu już zarezerwowany."
            context = {
                'title':title,
                'form':form
            }
            return render(request, 'reservation.html', context)



        else:
            title = "Proszę o poprawne wypełnienie formularza."
            context = {
                'form':form,
                'title':title
            }
            return render(request, 'reservation.html', context)

    return render(request, 'reservation.html', context)




def delete(request, pk):
    Reservation.objects.get(id=pk).delete()
    queryset = Reservation.objects.all()
    context = {
        'queryset':queryset
    }
    return render(request, "reservations.html", context)