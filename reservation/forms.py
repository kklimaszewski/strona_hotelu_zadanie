# -*- coding: utf-8 -*-
__author__ = 'karol'

from django import forms
from .models import Reservation
from django.utils.translation import ugettext as _

class ReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation
        fields = ['room_type', 'start_reservation', 'end_reservation', 'user_name', 'user_pesel']
        widgets = {
                'start_reservation': forms.DateInput(attrs={'id':'start_reservation', 'readonly':'true', 'placeholder':'Proszę zaznaczyć datę.'}),
                'end_reservation': forms.DateInput(attrs={'id':'end_reservation', 'readonly':'true', 'placeholder':'Proszę zaznaczyć datę.'}),
                'room_type': forms.HiddenInput()
            }
        error_messages = {
            'start_reservation': {
                'required': _(u"Te pole jest obowiązkowe!"),
            },
            'end_reservation': {
                'required': _(u"Te pole jest obowiązkowe!"),
            },
            'user_name': {
                'required': _(u"Te pole jest obowiązkowe!"),
            },
            'user_pesel': {
                'required': _(u"Te pole jest obowiązkowe!"),
            },
        }

