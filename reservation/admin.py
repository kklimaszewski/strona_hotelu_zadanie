from django.contrib import admin

from .models import Reservation

class ReservationAdmin(admin.ModelAdmin):
    list_display = ["room_type", "start_reservation", "end_reservation"]

admin.site.register(Reservation, ReservationAdmin)

