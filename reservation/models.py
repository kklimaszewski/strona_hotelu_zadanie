# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models

from apartment.models import Apartment



class Reservation(models.Model):
    room_type = models.ForeignKey(Apartment)
    start_reservation = models.DateField(blank=False, null=False)
    end_reservation = models.DateField(blank=False, null=False)
    user_name = models.CharField(max_length=50, null=True, blank=False)
    user_pesel = models.CharField(max_length=11, null=True, blank=False, validators=[RegexValidator(regex='^.{11}$', message='Pesel musi mieć dokładnie 11 liczb.', code='nomatch')])

    def __unicode__(self):
        return str(self.room_type)


    def clean(self):
        if self.end_reservation and self.start_reservation:
            if self.end_reservation <= self.start_reservation:
                raise ValidationError("Data koncowa musi być po dacie początkowej!")

