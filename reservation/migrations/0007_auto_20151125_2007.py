# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0006_auto_20151125_1905'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='room_reservation',
            field=models.ForeignKey(to='apartment.Apartment'),
        ),
    ]
