# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0003_auto_20151125_1415'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='user_pesel',
            field=models.CharField(max_length=11, null=True, validators=[django.core.validators.RegexValidator(regex=b'^.{11}$', message=b'Pesel length has to be 11', code=b'nomatch')]),
        ),
    ]
