# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0002_auto_20151125_1400'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservation',
            name='user_name',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='reservation',
            name='user_pesel',
            field=models.CharField(max_length=11, null=True),
        ),
    ]
