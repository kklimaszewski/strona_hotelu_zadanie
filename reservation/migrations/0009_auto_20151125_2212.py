# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0008_auto_20151125_2020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='end_reservation',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='start_reservation',
            field=models.DateField(),
        ),
    ]
