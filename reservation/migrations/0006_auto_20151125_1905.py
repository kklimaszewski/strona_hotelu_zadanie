# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0005_auto_20151125_1903'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='room_reservation',
            field=models.OneToOneField(to='apartment.Apartment'),
        ),
    ]
