# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0007_auto_20151125_2007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='end_reservation',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='start_reservation',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
