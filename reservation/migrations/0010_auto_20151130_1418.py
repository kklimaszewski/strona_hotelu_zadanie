# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0009_auto_20151125_2212'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reservation',
            old_name='room_reservation',
            new_name='room_type',
        ),
    ]
