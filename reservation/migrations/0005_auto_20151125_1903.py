# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0004_auto_20151125_1435'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='room_reservation',
            field=models.ForeignKey(to='apartment.Apartment', unique=True),
        ),
    ]
