# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0010_auto_20151130_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='end_reservation',
            field=models.DateField(error_messages={b'required': b'To pole nie mo\xc5\xbce by\xc4\x87 puste!'}),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='start_reservation',
            field=models.DateField(error_messages={b'required': b'To pole nie mo\xc5\xbce by\xc4\x87 puste!'}),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='user_name',
            field=models.CharField(max_length=50, null=True, error_messages={b'required': b'To pole nie mo\xc5\xbce by\xc4\x87 puste!'}),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='user_pesel',
            field=models.CharField(max_length=11, null=True, validators=[django.core.validators.RegexValidator(regex=b'^.{11}$', message=b'Pesel musi mie\xc4\x87 dok\xc5\x82adnie 11 liczb.', code=b'nomatch')]),
        ),
    ]
